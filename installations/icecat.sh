echo "deb http://mirror.fsf.org/trisquel/ belenos-updates main" | sudo tee -a /etc/apt/sources.list.d/trisquel.list

sudo gpg --keyserver keys.gnupg.net --recv-keys 0xE6C27099CA21965B734AEA31B4EFB9F38D8AEBF1
sudo gpg2 --export --armor B4EFB9F38D8AEBF1 | sudo apt-key add -

sudo touch /etc/apt/preferences.d/trisquel
echo "package: *" | sudo tee -a /etc/apt/preferences.d/trisquel
echo Pin: origin "mirror.fsf.org" | sudo tee -a /etc/apt/preferences.d/trisquel
echo "Pin-Priority: 100" | sudo tee -a /etc/apt/preferences.d/trisquel

sudo apt-get update
sudo apt-get install icecat
