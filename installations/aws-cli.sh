# Install PIP
$ sudo apt-get install python-pip python-dev build-essential
$ sudo pip install --upgrade pip
 
# Install AWS-CLI
pip install awscli --upgrade
 
# Verify the installation
aws --version

aws configure
