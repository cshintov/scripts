sudo apt-get install -y wget
mkdir /tmp/openssl
cd /tmp/openssl
sslversion='openssl-1.1.1f'
wget https://www.openssl.org/source/$sslversion.tar.gz
tar xvf $sslversion.tar.gz
cd /tmp/openssl/$sslversion
./config
make
sudo make install
