sudo apt-get install exuberant-ctags cmake libevent-dev libncurses5-dev
wget https://github.com/tmux/tmux/releases/download/2.6/tmux-2.6.tar.gz
tar -xvf tmux-2.6.tar.gz
cd tmux-2.6
./configure && make
sudo make install
