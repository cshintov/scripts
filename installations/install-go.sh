sudo apt-get update
sudo apt-get -y upgrade
wget https://dl.google.com/go/go1.12.2.linux-amd64.tar.gz
sudo tar -xvf go1.12.2.linux-amd64.tar.gz
sudo mv go /usr/local

echo "export GOROOT=/usr/local/go" >> ~/.bashrc
echo "GOPATH=$HOME/.go/" >> ~/.bashrc
echo "PATH=$GOPATH/bin:$GOROOT/bin:$PATH" >> ~/.bashrc
