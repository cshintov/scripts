sudo apt-get update
sudo apt-get install build-essential fakeroot dpkg-dev libcurl4-openssl-dev
sudo apt-get build-dep git
mkdir ~/git-openssl
cd ~/git-openssl
apt-get source git

read -p 'key? :' key # enter the key 

gpg --keyserver keyserver.ubuntu.com --recv-keys $key

# Then export the key to your local trustedkeys to make it trusted
gpg --no-default-keyring -a --export $key | gpg --no-default-keyring --keyring ~/.gnupg/trustedkeys.gpg --import -

ls
read -p 'key? :' git_package

dpkg-source -x $git_package 
cd $git_package

# replace all instances of libcurl4-gnutls-dev with libcurl4-openssl-dev in debian/control file

# Then build the package (if it's failing on test, you can remove the line TEST=test from the file debian/rules)
sudo dpkg-buildpackage -rfakeroot -b

# Install new package:

ls *[64\|86]*
read -p 'debpackage? :' debpack # enter the key 

sudo dpkg -i ../$debpack
