mkdir -p ~/.vim/autoload ~/.vim/bundle
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
# updating your vimrc file (assuming it is in ~/.vimrc)
touch ~/.vimrc
cp ~/.vimrc ~/.vimrc_bkup
# vundle.conf contains some plugins I use, remove the ones you dont want from plugin list
status=cat vundle.conf ~/.vimrc > ~/.vimrc
if [ "$status" -ne 0 ]; then
    echo 'you dont have vundle.conf! Dont worry I am downloading it!'
    wget https://bitbucket.org/cshintov/scripts/raw/c6fed6aed92b6c747bdef080792a9f094a753d5b/installations/vundle.conf
    cat vundle.conf ~/.vimrc > ~/.vimrc
fi
vim +PluginInstall +qall
