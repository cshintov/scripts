# mono is needed to run MarcEdit
# these commands were found from http://www.mono-project.com/docs/getting-started/install/linux/
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
echo "deb http://download.mono-project.com/repo/debian wheezy main" | sudo tee /etc/apt/sources.list.d/mono-xamarin.list
echo "deb http://download.mono-project.com/repo/debian wheezy-apache24-compat main" | sudo tee -a /etc/apt/sources.list.d/mono-xamarin.list
sudo apt-get update
sudo apt-get install mono-complete
sudo apt-get install libmono-system-windows-forms4.0-cil


# to install MarcEdit download the zip file to Downloads directory and unzip to /opt directory
chdir ~/Downloads
wget http://marcedit.reeset.net/software/marcedit.bin.zip
unzip marcedit.bin.zip -d /opt/
