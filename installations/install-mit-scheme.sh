# I like to put the compiled scheme files inside the directory I use
# for the class. Another reasonable option would be to use some
# temporary directory for SCHEME_DOWNLOAD and /usr/local for
# SCHEME_INSTALL
SCHEME_DOWNLOAD=~/shinto/softwares
SCHEME_INSTALL=/usr/local

mkdir -p $SCHEME_DOWNLOAD
cd $SCHEME_DOWNLOAD

# For 32 bit GNU/Linux system, use mit-scheme-9.2-i386.tar.gz
# instead of mit-scheme-9.2-x86-64.tar.gz here.
wget -c http://ftp.gnu.org/gnu/mit-scheme/stable.pkg/9.2/mit-scheme-9.2-x86-64.tar.gz

tar xf ./mit-scheme-9.2-x86-64.tar.gz

cd ./mit-scheme-9.2/src
# This prepares the build system for scheme and tells it to install
# the binaries that will be created to SCHEME_INSTALL.
./configure --prefix=$SCHEME_INSTALL

# This does the actual building of mit-scheme.
# Note: Some systems may complain about missing m4
#   This macro processor dependency can be downloaded from
#   https://www.gnu.org/software/m4/ or installed via your
#   system's package manager: e.g. sudo apt-get install m4 
make -j9 compile-microcode

# And this copies the installed files to SCHEME_INSTALL.
make install

# Try out your new mit-scheme system.
cd $SCHEME_INSTALL/bin

./mit-scheme --version
