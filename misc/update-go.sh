VERSION=1.14.2
BINARY=go$VERSION.linux-amd64.tar.gz
RELEASE=https://dl.google.com/go/$BINARY
CURRENT=/usr/local

echo "Current version"
go version

sudo rm -r $CURRENT/go/

wget $RELEASE

sudo tar -C $CURRENT -xzf $BINARY

echo "New version"
go version

echo "Current binary"
which go

rm $BINARY
