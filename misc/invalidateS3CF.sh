# aws cloudfront create-invalidation --distribution-id E13E0XKQ8QJSHT --paths  "/*"

DISTRIBUTION_IDS=( 
    E26Q5FCQZTA4GV
    E13E0XKQ8QJSHT
)

for dist_id in "${DISTRIBUTION_IDS[@]}";
do
  echo '***********'
  echo "$dist_id"
  aws cloudfront create-invalidation --distribution-id "$dist_id" --paths  "/*"
  echo '***********'
done

