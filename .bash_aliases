alias f='sudo shutdown now'
alias cploc='pwd | pbcopy'


# git aliases
alias gtfo='poweroff -now'
alias gb='git branch'
alias grb='git branch -r'
alias gd='git diff'
alias gm='git merge origin'
alias add-commit='git add -A :/ && git commit -m'
alias add='git add'
alias commit='git commit -m'
alias push='git push'
alias gs='git status'
alias gp='git pull'
alias gl='git log'
alias commend='git add -A && git commit --amend && git push origin HEAD -f'


# others
alias q='quit'
alias t='tmux'
alias pbcopy='xclip -selection clipboard'
alias pbpaste='xclip -selection clipboard -o'
alias upgpip='pip install --upgrade pip'
alias bat='upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -E "state|to\ full|percentage|time\ to\ empty|capacity"'
alias install='sudo apt-get install'
alias evimrc='vim ~/.vimrc'
alias listf='compgen -A function'
alias open='xdg-open'
alias remswap='find -name ".*.sw?" -delete'
alias quit='guake -q'
alias hipchat='hipchat&'
alias mvim='vim -p'
alias x='exit'
alias py='python'
alias deact='deactivate'
alias sentact='source ~/shinto/virtualenvs/sentinel/bin/activate'
alias ma='source ~/shinto/virtualenvs/marcenv/bin/activate'
alias me='mono /opt/marcedit/MarcEdit.exe&'
alias rmf='rm -f'
alias rmd='rm -rf'
alias rmrf='rm -rf'
alias cpd='cp -r'
alias pd='pushd'
alias c='clear'
alias bk='cd ..; ls'
alias als='vim ~/.bash_aliases'
alias uals='source ~/.bash_aliases'
alias gg='ping www.google.com'

cl() {
    cd $1;
    ls
}


fetch_branch(){
    git fetch $1 $2:$3;
    git checkout $2
}


# startbitbucket - creates remote bitbucket repo and adds it as git remote to cwd
function createbbrepo {
    read -p 'Repo name?' reponame
    read -p 'is_private' is_private #true or false
    mkdir $reponame
    cd $reponame
    git init
    echo my project $reponame > README
    git add -A
    git commit -m "Initial Commit"
    read -p 'Username?' username
    read -p 'Password?' -s password  # -s flag hides password text
    echo 'creating repo'
    curl --user $username:$password https://api.bitbucket.org/1.0/repositories/ --data name=$reponame --data is_private='$isprivate'
    echo 'adding remote origin'
    git remote add origin git@bitbucket.org:$username/$reponame.git
    echo 'pushing'
    git push -u origin --all
    git push -u origin --tags
}


function createrepo {
    reponame=$1
    is_private=$2
    mkdir $reponame
    cd $reponame
    git init
    echo my project initialization $reponame > README
    git add -A
    git commit -m "Initial Commit"
    read -p 'Password?' -s password  # -s flag hides password text
    echo
    echo 'creating repo'
    curl --user cshintov:$password https://api.bitbucket.org/2.0/repositories/cshintov/$reponame --data is_private='$isprivate'
    echo 'adding remote origin'
    git remote add origin git@bitbucket.org:cshintov/$reponame.git
    echo 'pushing'
    git push -u origin --all
    git push -u origin --tags
}


function delrepo (){
    read -r -p "Are you sure? [y/N] " response
    read -p 'Password?' -s password  # -s flag hides password text
    case $response in
        [yY][eE][sS]|[yY]) 
        echo
        echo deleting the repo $1 .....
        curl -X DELETE --user cshintov:$password https://api.bitbucket.org/2.0/repositories/cshintov/$1
        rm -rf $1
            ;;
        *)
            exit 1
            ;;
    esac
}


function pushall {
    if [ -z "$1" ]; then
        read -p 'to which branch? ' branch
    else
        branch=$1
    fi
    read -p 'commit message: ' message
    add-commit "$message"
    if [ "$?" -ne 0 ]; then
        echo 'error committing!'
    else
        echo 'successfully committed!'
    fi
    push origin $branch
}


function frb {
    if [ "$1" == "" ]; then
        read -p 'branch? ' branch
    else
        branch=$1
    fi

    git fetch origin $branch
    git checkout $branch
}


function xfile {
    touch $1
    chmod +x $1
    vim $1
}


function junk {
    mv $1 ~/shinto/junk
}


function dload {
    for link in `cat $1`
        do
        youtube-dl $link
        done
}

function wload {
    for link in `cat $1`
        do
        wget $link
        done
}

function readf {
    while IFS='' read -r line || [[ -n "$line" ]]; do
        echo "Text read from file: $line"
        done < "$1"
}

alias sentact='source ~/shinto/virtualenvs/sentinel/bin/activate'
alias ma='source ~/shinto/virtualenvs/marcenv/bin/activate'


function act {
    source ~/shinto/virtualenvs/$1/bin/activate;
}

function lsvenv {
    ls ~/shinto/virtualenvs;
}
