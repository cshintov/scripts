set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'kshenoy/vim-signature'
Plugin 'majutsushi/tagbar'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-repeat'
Plugin 'kien/ctrlp.vim'
Plugin 'plasticboy/vim-markdown'
Plugin 'sjl/gundo.vim'
" Plugin 'Valloric/YouCompleteMe'
Plugin 'scroolose/syntastic'
Plugin 'scroolose/nerdtree'
Plugin 'altercation/vim-colors-solarized'
Plugin 'Lokaltog/powerline'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Avoid a name conflict with L9
" Plugin 'user/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
set showmatch
set autoindent
set shiftwidth=4
set expandtab
set tabstop=4
set nu
syntax on
execute pathogen#infect()
onoremap p i(
onoremap ,br i{
onoremap B /return/e<cr>
nnoremap <silent> <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>
set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class
set pastetoggle=<F10>
map <C-n> :NERDTreeToggle<CR>
nmap <F8> :TagbarToggle<CR>
map <F3> :w<CR>
" disabling youcomplteme plugin
set runtimepath-=~/.vim/bundle/YouCompleteMe
set hidden
let mapleader = ','
noremap <Leader>e $
noremap <Leader>b ^
inoremap jj <ESC>
inoremap <Leader>ww <ESC>:w<CR>
noremap <Leader>w :w<CR>
noremap <Leader>q :wq<CR>
noremap <Leader>x :q<CR>
noremap <Leader>a :qa<CR>
noremap <Leader>tn :tabnew 
noremap <Leader>cm :s/^/# /g<CR>
noremap <Leader><Leader>c 0wi# <ESC>
noremap <Leader>sc :source ~/.vimrc<CR>
noremap <Leader>rc :tabnew ~/.vimrc<CR>
noremap <Leader>rl :edit!<CR>
noremap <Leader>sh :shell<CR>
noremap <Leader>stm :SyntasticToggleMode<CR>

noremap <Leader>g G
noremap <Leader>n gt
noremap <Leader>m gT

noremap <Leader>z "ap
noremap <Leader>y "bp
