from functools import wraps

def handle_errors(*errors):
    def inner(func):
        @wraps(func)
        def func_wrapper(name):
            try:
                return func(name)
            except errors as err:
                print err
        return func_wrapper
    return inner


@sns_failures(Exception)
def get_text(name):
    """returns some text"""
    k = 'a' + 2
    raise Exception
    return "Hello " + name


get_text('shinto')
