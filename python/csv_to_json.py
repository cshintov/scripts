import csv
import json
import sys


def csv_2_json(csvfile, jsonfile):
    """ convert csvfile to jsonfile """
    with open(csvfile) as csvfile, open(jsonfile, 'w') as jsonfile:
        fieldnames = [
            fname.strip('"') for fname in csvfile.readline().strip().split(',')
        ]
        reader = csv.DictReader(csvfile, fieldnames)
        for row in reader:
            json.dump(row, jsonfile)
            jsonfile.write('\n')


if __name__ == '__main__':
    csvfile, jsonfile = sys.argv[1:]
    csv_2_json(csvfile, jsonfile)
